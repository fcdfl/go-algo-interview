package reverse

import "github.com/isdamir/gotype"

// 就地逆序
func SituReverse(node *gotype.LNode) {
	if node == nil || node.Next == nil {
		return
	}
	var pre, cur *gotype.LNode
	next := node.Next
	for next != nil {
		cur = next.Next
		next.Next = pre
		pre = next
		next = cur
	}
	node.Next = pre
}
