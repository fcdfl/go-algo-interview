package reverse

import (
	"testing"

	"github.com/isdamir/gotype"
)

func TestSitu(t *testing.T) {
	head := &gotype.LNode{}
	t.Log("就地逆序")
	gotype.CreateNode(head, 8)
	gotype.PrintNode("逆序前：", head)
	SituReverse(head)
	gotype.PrintNode("逆序后：", head)
}

func TestRecursiveReverse(t *testing.T) {
	head := &gotype.LNode{}
	t.Log("递归逆序")
	gotype.CreateNode(head, 8)
	gotype.PrintNode("逆序前：", head)
	RecursiveReverse(head)
	gotype.PrintNode("逆序后：", head)
}
func TestInsertReverse(t *testing.T) {
	head := &gotype.LNode{}
	t.Log("插入法逆序")
	gotype.CreateNode(head, 8)
	gotype.PrintNode("逆序前：", head)
	InsertReverse(head)
	gotype.PrintNode("逆序后：", head)
}
